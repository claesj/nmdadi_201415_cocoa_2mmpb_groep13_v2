##Table of contents
- Productiedossier Briefing 
    - NMDAD-I 2014-2015 
        - Productiedossier Briefing 
    - Studiemateriaal
    - tools
    - technologie&euml;n
    - inhoud
- analyse
    - technische specificaties
    - functionele specificaties
- Persona&lsquo;s
- moodboard
- sitemap
- wireframes
- screenshots

#Tussentijds productiedossier (milestone 1)
##Briefing

NMDAD-I 2014-15
===============

|Info||
|----|-|
|Auteur(s)|Philippe De Pauw - Waterschoot|
|Opleiding|Grafische en Digitale Media|
|Academiejaar|2014-15|

***


|Info||
|--|--|
|Studiepunten|6|
|Contacturen|4|
|Docent|Philippe De Pauw - Waterschoot|
|Evaluatie 1ste examenkans|Werkstuk(ken) (maximaal in groepjes van 3). Mondelinge verdediging van werkstuk(ken) + vragen|
|Evaluatie 2de examenkans|Werkstuk(ken) (Individueel). Mondelinge verdediging van werkstuk(ken) + vragen|
|Begincompetenties|2D Animation & Webdesign II|
|ECTS|[ECTS Fiche NMDADI](http://www.arteveldehogeschool.be/ects/ahsownapp/ects/ECTSFiche.aspx?olodID=42086)|


##Inhoud

- **Inleiding**
- **Markdown**
- Dataformaten: CSV, XML, XSL-T, XPath, XSL-FO, JSON(P), BSON, SVG, SMIL, MathML
- **Mappenstructuur van een front-end webapplicatie**
- HTML5: Global attributes, Semantische elementen, Geolocation, Canvas
- Responsive webdesign, mobile first/desktop first (zonder framework)
- Responsive navigations, images, video's en andere componenten
- Icons: Favicon, Touch icons, Sprites
- Font iconen
- Uitbreiding CSS3
- Prototype-based programming (OOP)
- AMD (Asynchronous Module Definition)
- Consuming Data met JavaScript
- User Experience (UX) Design

##Studiemateriaal

- Chamilo cursus NMDAD-I
- Bitbucket Respository [NMDADI_201415](https://bitbucket.org/drdynscript/nmdadi_201415)
- Online links, tutorials and samples

##Tools

- <http://git-scm.com/>
- <http://www.sourcetreeapp.com/>
- <http://www.jetbrains.com/phpstorm/>
- <https://bitbucket.org/>
- <http://courses.olivierparent.be/servermanagement/local-development-environment/installing-design-and-development-tools/source-control/>

##Technologie&euml;n

- HTML
- CSS
- JavaScript
- CSS pre-processors
- Static Site Generators
- Commandline and Automation tools
- JS bibliotheken & frameworks, zoals: jQuery, Normalize, Modernizr, Respond.js, Underscore.js, Lodash.js, Crossroads.js, Hashes.js, ...


***
###Inhoud
####Inleiding
>Het maken van een responsive web-app, die lijkt de look en feel van een native app heeft.
Wij kozen voor het mobile-first ontwerpen.

##Analyse

###Technische specificaties

> - Versiebeheer
- Maak een nieuw Git Repository voor je project aan op Bitbucket. Code worden tussen groepen niet gedeeld! Deel het project met de docent(en).
- Frontend
- HTML5, CSS3 en JavaScript
- jQuery, underscore.js, lodash.js, crossroads.js, js-signals
- localstorage of IndexedDB
- Webapplicatie moet de look-and-feel van een native applicatie! Het responsive- framework alsook alle andere bestanden moeten zelf geÃ¯mplementeerd worden. Automation in dit project is niet noodzakelijk, kortom de dist-folder en - de test-folder kunnen leeg zijn!

http://www.pttrns.com/
http://www.mobile-patterns.com/


###Functionele specificaties

> - User kan lijsten beheren
- User kan tags beheren
- User kan tags toekennen aan een lijst
- User kan taken toekennen aan een lijst
- User kan taken beheren
- User kan taken voltooien en deze voltooing ongedaan maken
- User kan prioriteiten instellen voor de taken binnen een lijst en voorziet kleurcodes per prioriteit
- User kan due-date beheren voor iedere taak
- User beheert reminders


#PERSONA&lsquo;S

###Studenten
- Profiel:
    - Studenten van hogeschool/universiteit
    - Twintiger
    - Zowel man als vrouw
    - Beschikt over smartphone/tablet en pc
- Behoeften:
    - Bijhouden van taken
    - Bijhouden van examens/testen
    - Voorbereidingen maken
    - Planning opstellen
- Realisatie van behoeften door onze app:
    - App kan zowel thuis als op school gebruikt worden om opgegeven taken in te plannen tegen bepaalde einddatum en kunnen bij voltooiing worden verwijderd.
    - App kan belangrijke gebeurtenissen en datums bijhouden
    - App is simpel in gebruik
- Gebruik van app
    - Voornamelijk smartphone of tablet
    - Kan ook via pc



###Ouder(s) in een gezin met kinderen
- Profiel   
     - Maken deel uit van een huishouden
     - tussen 30 en 40 jaar
     - Zowel man als vrouw
     - Beschikt over smartphone/tablet
- Behoeften:
    - Bijhouden van klusjes in huishouden
    - Bijhouden van boodschappenlijsten
    - Weten waar en wanneer kinderen moeten worden opgehaald
    - Belangrijke gebeurtenissen bijhouden (vb. Verjaardagen, trouwdatum, …
- Realisatie van behoeften door onze app:
     - App kan lijsten bijhouden op mobiel apparaat en kunnen achteraf overal worden geraadpleegd.
      - Vb. Thuis opstellen van boodschappenlijstje op smartphone, smartphone wordt meegenomen naar winkel en lijstje kan worden geraadpleegd.
      - App is simpel en handig in gebruik
- Gebruik van app
     - Voornamelijk smartphone of tablet


###Vertegenwoordiger van bedrijf die huisbezoeken doet
 - profiel
    - 40j
    - Zowel man als vrouw
    - Beschikt over smartphone/tablet
- Behoeften:
    - Bijhouden van adressen die bezocht moeten worden
    - Bijhouden van uurrooster
    - Gegevens potentiële klanten opnemen
- Realisatie van behoeften door onze app:
    - App bevat lijst met alle adressen die bezocht worden, geïnteresseerden kunnen in andere lijst worden gekopieerd
    -   App is simpel en handig in gebruik
- Gebruik van app
    - Smartphone of tablet
 
 
MOODBOARD
===============

<img src="images/moodboard.jpg">

SITEMAP
===============
<img src="images/sitemap.jpg">

WIREFRAMES
===============

##Smartphone-Versie

###Toevoegen van items
<img src="images/w01_sma.jpg">


###Home-screen
<img src="images/w02_sma.jpg">

<img src="images/w03_sma.jpg">

<img src="images/w04_sma.jpg">

<img src="images/w05_sma.jpg">

##Tablet-Versie

###Toevoegen van items
<img src="images/w01_tab.jpg">

###Home-screen
<img src="images/w02_tab.jpg">

<img src="images/w03_tab.jpg">

<img src="images/w04_tab.jpg">

<img src="images/w05_tab.jpg">

##Desktop-Versie

###Home-screen
<img src="images/w01_des.jpg">

###Home-screen
<img src="images/w02_des.jpg">
<img src="images/w03_des.jpg">
<img src="images/w04_des.jpg">
<img src="images/w05_des.jpg">

SCREENSHOTS
===============