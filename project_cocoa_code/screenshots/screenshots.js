var wp = require('webpage'), page,
    system = require('system');

var url = ('../app/index.html'),
    breakpoints = [320, 480, 640, 800, 960, 1024, 1280],
    i = 0;
createScreenshotNextResolution(breakpoints[i]);

function createScreenshotNextResolution(w){
    page = wp.create();
    page.viewportSize = {width:w, height:800};
    page.clipRect = {//cut result
        top:0,
        left:0,
        width:w,
        height:800
    };

    page.open(url, function(p){
        if(p !== 'success'){
            console.log('Failed to load the webpage: ' + url +'!');
        }else{
            var t = new Date().getTime();

            var title = page.evaluate(function(){
                return document.title;
            });

            var scn = title.split(' ')[0] + "_" + t + "_" + w + ".png";

            page.render(scn);

            console.log('Created the screenshot for url: ' + url + ' with the viewportwidth ' + w + 'px --> Name: ' + scn);
        }

        if(i+1<breakpoints.length){
            i++;
            createScreenshotNextResolution(breakpoints[i]);
        }else{
            phantom.exit();
        }
    });
}