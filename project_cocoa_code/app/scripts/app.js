/*
 * Created by: Jens Claes & Stijn De Rammelaere
 * Last Update: 14-01-2015
 * Name: app.js
 * Project: DoYouRemember?!
 * Description: Webapp - To Do List
 * Site: www.doyouremember.eu
 */

window.onload = function () {
    'use strict';

    /* DOM elements */
    var popup = document.querySelector('#popup-wrapper');
    var feedback = document.querySelector('#feedback-wrapper');
    var knop_bevestig = document.querySelector('#btn-popup-confirm');
    var knop_anuleer = document.querySelector('#btn-popup-close');
    var feedback_confirm = document.querySelector('#btn-feedback-confirm');
    var navigation = document.querySelector('#navigation-wrapper');
    var tekstveld = document.querySelector('#popup-text');
    var feedbackText = document.querySelector('#feedback-text');
    var toggleNewTask = document.querySelector('#add-new-task');
    var toggleNewList = document.querySelector('#add-new-list');
    var formNewTask = document.querySelector('#formNewTask');
    var nameNewTask = document.querySelector('#txtNewTask');
    var listSelection = document.querySelector('#selectList');
    var catNewTask =document.querySelector('#txtNewTaskTags');
    var levels = document.querySelectorAll('.pointer');
    var btnPriorityZero = document.querySelector('#zero');
    var btnPriorityOne = document.querySelector('#one');
    var btnPriorityTwo = document.querySelector('#two');
    var btnPriorityTree = document.querySelector('#tree');
    var btnPriorityFour = document.querySelector('#four');
    var noteNewTask = document.querySelector('#txtOpmerking');
    var locationNewTask = document.querySelector('#txtLocatie');
    var btnToggleDeadline = document.querySelector('#toggle-deadline-button');
    var timeField = document.querySelector('#timeField');
    var timePicker = document.querySelector('#timePicker');
    var datePicker = document.querySelector('#datePicker');
    var btnToggleReminder = document.querySelector('#toggle-reminder-button');
    var reminderField = document.querySelector('#reminderField');
    var reminderValue = document.querySelector('#reminder-value');
    var newTaskSubmit= document.querySelector('#btnTaskSubmit');
    var newTaskClose= document.querySelector('#btnTaskClose');
    var containerAllElements = document.querySelector('#elements-all');
    var formNewList = document.querySelector('#formNewList');
    var nameNewList = document.querySelector('#txtNewList');
    var tagsNewList = document.querySelector('#txtNewListTags');
    var btnListSubmit = document.querySelector('#btnListSubmit');
    var btnListClose = document.querySelector('#btnListClose');
    var taskEditor = document.querySelector('#task-editor');
    var formEditTask = document.querySelector('#formEditTask');
    var nameEditTask = document.querySelector('#txtEditTask');
    var catEditTask = document.querySelector('#txtEditTaskTags');
    var editListSelection = document.querySelector('#selectEditList');
    var btnPriorityZeroEdit = document.querySelector('#zero-edit');
    var btnPriorityOneEdit = document.querySelector('#one-edit');
    var btnPriorityTwoEdit = document.querySelector('#two-edit');
    var btnPriorityTreeEdit = document.querySelector('#tree-edit');
    var btnPriorityFourEdit = document.querySelector('#four-edit');
    var noteEditTask = document.querySelector('#txtEditOpmerking');
    var locationEditTask = document.querySelector('#txtEditLocatie');
    var btnToggleEditDeadline = document.querySelector('#toggle-deadline-edit-button');
    var editTimeField = document.querySelector('#timeFieldEdit');
    var editTimePicker = document.querySelector('#timePickerEdit');
    var editDatePicker = document.querySelector('#datePickerEdit');
    var btnToggleEditReminder = document.querySelector('#toggle-reminder-edit-button');
    var editReminderField = document.querySelector('#reminderFieldEdit');
    var editReminderValue = document.querySelector('#reminder-value-edit');
    var editReminderValueType = document.querySelector('#reminder-value-type-edit');
    var btnSubmitEditTask = document.querySelector('#btnEditTaskSubmit');
    var btnCloseEditTask = document.querySelector('#btnEditTaskClose');
    var listEditor = document.querySelector('#list-editor');
    var nameEditList = document.querySelector('#txtEditList');
    var tagsEditList = document.querySelector('#txtEditListTags');
    var btnSubmitEditList = document.querySelector('#btnEditListSubmit');
    var btnCloseEditList = document.querySelector('#btnEditListClose');
    var clearStorage = document.querySelector('#btnDeleteLocalStorage');
    var popupReminder = document.querySelector('#toggleReminderPopUp');
    var popupDelete = document.querySelector('#toggleDeletePopUp');
    var popupStorage = document.querySelector('#toggleStoragePopUp');
    var reminderCon = document.querySelector('#reminders-all');
    /* End DOM elements */

    /* No HTML5 DatePicker */
    if(!Modernizr.inputtypes.date)
    {
        new Pikaday(
        {
            field: document.getElementById('datePicker'),
            format: 'YYYY-MM-DD',
            firstDay: 1,
            i18n: {
                previousMonth : 'Vorige maand',
                nextMonth     : 'Volgende maand',
                months        : ['Januari','Februari','Maart','April','Mei','Juni','Juli','Augustus','September','Oktober','November','December'],
                weekdays      : ['Zondag','Maandag','Dinsdag','Woensdag','Donderdag','Vrijdag','Zaterdag'],
                weekdaysShort : ['Zo','Ma','Di','Wo','Do','Vr','Za']
            }
        });
        new Pikaday(
            {
                field: document.getElementById('datePickerEdit'),
                format: 'YYYY-MM-DD',
                firstDay: 1,
                i18n: {
                    previousMonth : 'Vorige maand',
                    nextMonth     : 'Volgende maand',
                    months        : ['Januari','Februari','Maart','April','Mei','Juni','Juli','Augustus','September','Oktober','November','December'],
                    weekdays      : ['Zondag','Maandag','Dinsdag','Woensdag','Donderdag','Vrijdag','Zaterdag'],
                    weekdaysShort : ['Zo','Ma','Di','Wo','Do','Vr','Za']
                }
            });
    }
    /* End no HTML5 DatePicker */

    /* App constructor */
    var App = {
        init: function() {
            this.todoDBContext = TodoDBContext;
            this.todoDBContext.init('jc.doyouremember');
            this.currentPriority = 'zero';
            this.deadlineToggle = false;
            this.reminderToggle = false;
            this.setupRoutes();
            this.catchEvents();
            this.updateUI();
        },
        setupRoutes: function() {
            this.router = crossroads;
            this.hash = hasher;
            var self = this;
            var homeRoute = this.router.addRoute('/');
            homeRoute.matched.add(function() {
                self.onSectionMatch('home');
            });
            var sectionRoute = this.router.addRoute('/{section}');
            sectionRoute.matched.add(self.onSectionMatch);
            this.hash.initialized.add(function(newHash) {
                self.router.parse(newHash);
            });
            this.hash.changed.add(function(newHash) {
                self.router.parse(newHash);
            });
            this.hash.init();
        },
        onSectionMatch: function(section) {
            var pages = document.querySelectorAll('.page');
            if (pages !== null && pages.length > 0) {
                _.each(pages, function(page) {
                    if (page.id === section) {
                        setTimeout(function() {
                        page.classList.remove('hidden');
                        setTimeout(function() {
                            page.classList.add('current');
                        },20);
                        },270);
                    } else {
                        page.classList.remove('current');
                        setTimeout(function() {
                        page.classList.add('hidden');
                        },270);
                    }
                });
            }
            App.updateUI();
        },
        clearForms: function(){
            formNewTask.reset();
            formNewList.reset();
        },
        catchEvents: function(){
            btnPriorityZero.addEventListener('click', function() {
                setPriority('zero');
            });
            btnPriorityOne.addEventListener('click', function() {
                setPriority('one');
            });
            btnPriorityTwo.addEventListener('click', function() {
                setPriority('two');
            });
            btnPriorityTree.addEventListener('click', function() {
                setPriority('tree');
            });
            btnPriorityFour.addEventListener('click', function() {
                setPriority('four');
            });
            btnPriorityZeroEdit.addEventListener('click', function() {
                setPriority('zero');
            });
            btnPriorityOneEdit.addEventListener('click', function() {
                setPriority('one');
            });
            btnPriorityTwoEdit.addEventListener('click', function() {
                setPriority('two');
            });
            btnPriorityTreeEdit.addEventListener('click', function() {
                setPriority('tree');
            });
            btnPriorityFourEdit.addEventListener('click', function() {
                setPriority('four');
            });
            toggleNewTask.addEventListener('click', function() {
                formNewTask.classList.toggle('hidden');
                closeNewList();
                setPriority('zero');
                toggleAllOff();
            });
            toggleNewList.addEventListener('click', function() {
                formNewList.classList.toggle('hidden');
                closeNewTask();
            });
            btnToggleDeadline.addEventListener('click', toggleDeadline);
            btnToggleEditDeadline.addEventListener('click', toggleDeadline);
            btnToggleReminder.addEventListener('click', toggleReminder);
            btnToggleEditReminder.addEventListener('click', toggleReminder);
            newTaskSubmit.addEventListener('click', submitNewTask);
            newTaskClose.addEventListener('click', closeNewTask);
            btnListSubmit.addEventListener('click', submitNewList);
            btnListClose.addEventListener('click', closeNewList);
            clearStorage.addEventListener('click', clear);
            popupReminder.addEventListener('click', function() {
                App.todoDBContext.setFeedbackAlert(!App.todoDBContext.getFeedbackAlert());
                App.updateUI();
            });
            popupDelete.addEventListener('click', function() {
                App.todoDBContext.setDeleteAlert(!App.todoDBContext.getDeleteAlert());
                App.updateUI();
            });
            popupStorage.addEventListener('click', function() {
                App.todoDBContext.setLocalStorageAlert(!App.todoDBContext.getLocalStorageAlert());
                App.updateUI();
            });
        },
        updateUI: function() {
            App.clearForms();
            closeNewTask();
            closeNewList();
            createRemindersInContainer(reminderCon);
            var elements = this.todoDBContext.getElements();
            var allLists = [];
            if (elements !== null && elements.length > 0) {
                _.each(elements, function(element) {
                    if (element.type === 'list') {
                        allLists.push(element);
                    }
                });
                containerAllElements.classList.remove('hidden');
                createElementsInContainer(containerAllElements);
                var elementNodes = document.querySelectorAll('.element');
                if (elementNodes !== null && elementNodes.length > 0) {
                    _.each(elementNodes, function(elementNode) {
                        elementNode.querySelector('.element-remove').addEventListener('click', function() {
                            var elementId = this.parentNode.dataset.id;
                            if (App.todoDBContext.getDeleteAlert()) {
                                showPopup('Wil je het volgende item: ' + App.todoDBContext.getElement(elementId).name + ' verwijderen?',
                                    function (confirmation){
                                        if (confirmation){
                                            removeElementById(elementId);
                                        }
                                    });
                            }else{
                                removeElementById(elementId);}
                        });
                        elementNode.querySelector('.element-edit').addEventListener('click', function() {
                            var elementId = this.parentNode.dataset.id;
                            updateElement(App.todoDBContext.getElement(elementId));
                        });
                    });
                }
                var taskNodes = document.querySelectorAll('.task');
                if (taskNodes !== null && taskNodes.length > 0) {
                    _.each(taskNodes, function(taskNode) {
                        taskNode.querySelector('.task-icon').addEventListener('click', function() {
                            var taskId = this.parentNode.dataset.id;
                            toggleCompleteTask(App.todoDBContext.getElement(taskId));
                        });
                    });
                }
                var listNodes = document.querySelectorAll('.list');
                if (listNodes !== null && listNodes.length > 0) {
                    _.each(listNodes, function(listNode) {
                        listNode.querySelector('.list-icon').addEventListener('click', function() {
                            var listId = this.parentNode.dataset.id;
                            toggleListOpen(App.todoDBContext.getElement(listId));
                        });
                    });
                }
                var listedTaskNodes = document.querySelectorAll('.listed-task');
                if (listedTaskNodes !== null && listedTaskNodes.length > 0) {
                    _.each(listedTaskNodes, function(listedTaskNode) {
                        listedTaskNode.querySelector('.listed-task-icon').addEventListener('click', function() {
                            var taskId = this.parentNode.dataset.id;
                            var listId = this.parentNode.dataset.list;
                            toggleCompleteListedTask(App.todoDBContext.getListedElement(App.todoDBContext.getElement(listId), taskId), App.todoDBContext.getElement(listId));
                        });
                        listedTaskNode.querySelector('.listed-task-remove').addEventListener('click', function() {
                            var taskId = this.parentNode.dataset.id;
                            var listId = this.parentNode.dataset.list;
                            if (App.todoDBContext.getDeleteAlert()) {
                                showPopup('Wil je het volgende item: ' + App.todoDBContext.getListedElement(App.todoDBContext.getElement(listId), taskId).name + ' verwijderen?',
                                    function(confirmation){
                                        if(confirmation){
                                            removeListedElementById(App.todoDBContext.getElement(listId), taskId);
                                        }
                                    });
                            }else{removeListedElementById(App.todoDBContext.getElement(listId), taskId);}

                        });
                        listedTaskNode.querySelector('.listed-task-edit').addEventListener('click', function() {
                            var taskId = this.parentNode.dataset.id;
                            var listId = this.parentNode.dataset.list;
                            var task = App.todoDBContext.getListedElement(App.todoDBContext.getElement(listId), taskId);
                            updateListedTask(App.todoDBContext.getElement(listId), task);
                        });
                    });
                }
            } else {
                containerAllElements.classList.add('hidden');
            }
            //EDITING
            var htmlContent = '<option value="no-list" data-id="no-list">Geen lijst</option>';
            if (allLists !== null && allLists.length > 0) {
                _.each(allLists, function(list) {
                    htmlContent += '<option value="' + list.id + '" data-id="' + list.id + '">' + list.name + '</option>';
                });
            }
            //
            listSelection.innerHTML = htmlContent;
            editListSelection.innerHTML = htmlContent;

            if(App.todoDBContext.getFeedbackAlert()) {
                popupReminder.classList.remove('fa-toggle-off');
                popupReminder.classList.add('fa-toggle-on');
            }else{
                popupReminder.classList.add('fa-toggle-off');
                popupReminder.classList.remove('fa-toggle-on');
            }
            if(App.todoDBContext.getDeleteAlert()) {
                popupDelete.classList.remove('fa-toggle-off');
                popupDelete.classList.add('fa-toggle-on');
            }else{
                popupDelete.classList.add('fa-toggle-off');
                popupDelete.classList.remove('fa-toggle-on');
            }
            if(App.todoDBContext.getLocalStorageAlert()) {
                popupStorage.classList.remove('fa-toggle-off');
                popupStorage.classList.add('fa-toggle-on');
            }else{
                popupStorage.classList.add('fa-toggle-off');
                popupStorage.classList.remove('fa-toggle-on');
            }
        }
    };
    /* End App constructor */

    /* Confirm popup constructor */
    var showPopup = function(message, handler){
        function confirm(){
            handler(true);
            knop_bevestig.removeEventListener('click', confirm);
            knop_anuleer.removeEventListener('click', cancel);
            popup.classList.add('hidden');
            navigation.classList.remove('hidden');
        }
        function cancel(){
            handler(false);
            knop_bevestig.removeEventListener('click', confirm);
            knop_anuleer.removeEventListener('click', cancel);
            popup.classList.add('hidden');
            navigation.classList.remove('hidden');
        }
        knop_bevestig.addEventListener('click', confirm);
        knop_anuleer.addEventListener('click', cancel);
        popup.classList.remove('hidden');
        navigation.classList.add('hidden');
        tekstveld.innerHTML = message;
    };
    /* End Confirm popup constructor */

    /* Feedback popup constructor */
    var showFeedback = function(message){
        function confirm(){
            feedback_confirm.removeEventListener('click', confirm);
            feedback.classList.add('hidden');
            navigation.classList.remove('hidden');
        }
        if (App.todoDBContext.getFeedbackAlert())
        {
            feedback_confirm.addEventListener('click', confirm);
            feedback.classList.remove('hidden');
            navigation.classList.add('hidden');
            feedbackText.innerHTML = message;
        }
    };
    /* End Feedback popup constructor */

    /* Functions to submit new list */
    function submitNewList() {
        var list = new List();
        list.id = Utils.guid();
        list.name = nameNewList.value;
        if (list.name === '') {
            list.name = "Naamloze Lijst";
        }
        list.cat = tagsNewList.value;
        list.created = new Date().getTime();
        App.todoDBContext.addElement(list);
        window.location.href = '#/';

        showFeedback('Lijst: "' + list.name + '" is aangemaakt');

        App.updateUI();

    }
    /* End functions to submit new list */

    /* Functions to submit new task */
    function submitNewTask() {
        var selectedListId = formNewTask.listSelect.options[formNewTask.listSelect.selectedIndex].dataset.id;
        var task = new Task();
        task.id = Utils.guid();
        task.name = nameNewTask.value;
        if (task.name === '') {
            task.name = "Naamloze Taak";
        }
        task.priority = 'priority-' + App.currentPriority;

        task.note = noteNewTask.value;
        task.place = locationNewTask.value;

        task.created = new Date().getTime();
        if (App.deadlineToggle === true) {
            var hour = Utils.getHourFromTime(timePicker.value);
            var minutes = Utils.getMinutesFromTime(timePicker.value);
            task.endDate = new Date(datePicker.value + 'T' + hour + ':' + minutes+'Z');
            if (Object.prototype.toString.call(task.endDate) !== '[object Date]' || isNaN(task.endDate)) {
                task.endDate = "none";
            }
            toggleDeadline();
        } else {
            task.endDate = "none";
        }
        if (task.endDate !== "none" && App.reminderToggle === true) {
            var value = reminderValue.value;
            var valueType = formNewTask.remindervaluetype.options[formNewTask.remindervaluetype.selectedIndex].value;
            task.remindDate = task.endDate.getTime() - Utils.getTimeInMs(value, valueType);
            task.remindValue = value;
            task.remindType = valueType;
        } else {
            task.remindDate = "none";
        }
        if (selectedListId === 'no-list') {
            task.cat = catNewTask.value;
            App.todoDBContext.addElement(task);
        } else {
            var selectedList = App.todoDBContext.getElement(selectedListId);
            selectedList.open = true;
            App.todoDBContext.addListedElement(selectedList, task);
        }
        setPriority('zero');
        showFeedback('Taak: "' + task.name + '" is aangemaakt');
        App.updateUI();
    }
    function setPriority(priority) {
        if (levels !== null && levels.length > 0) {
            _.each(levels, function(level) {
                level.innerHTML = '';
            });
        }
        App.currentPriority = priority;
        var priorityLevel = '#' + priority;
        var priorityEdit = '#' + priority + '-edit';
        document.querySelector(priorityLevel).innerHTML = '<i class="fa fa-check"></i>';
        document.querySelector(priorityEdit).innerHTML = '<i class="fa fa-check"></i>';
    }
    function toggleDeadline() {
        if (App.deadlineToggle === false) {
            btnToggleDeadline.classList.remove('fa-toggle-off');
            btnToggleDeadline.classList.add('fa-toggle-on');
            timeField.classList.remove('hidden');
            btnToggleEditDeadline.classList.remove('fa-toggle-off');
            btnToggleEditDeadline.classList.add('fa-toggle-on');
            editTimeField.classList.remove('hidden');
            App.deadlineToggle = true;
        } else {
            btnToggleDeadline.classList.add('fa-toggle-off');
            btnToggleDeadline.classList.remove('fa-toggle-on');
            timeField.classList.add('hidden');
            btnToggleEditDeadline.classList.add('fa-toggle-off');
            btnToggleEditDeadline.classList.remove('fa-toggle-on');
            editTimeField.classList.add('hidden');
            App.deadlineToggle = false;
        }
    }
    function toggleReminder() {
        if (App.reminderToggle === false) {
            btnToggleReminder.classList.remove('fa-toggle-off');
            btnToggleReminder.classList.add('fa-toggle-on');
            reminderField.classList.remove('hidden');
            btnToggleEditReminder.classList.remove('fa-toggle-off');
            btnToggleEditReminder.classList.add('fa-toggle-on');
            editReminderField.classList.remove('hidden');
            App.reminderToggle = true;
        } else {
            btnToggleReminder.classList.add('fa-toggle-off');
            btnToggleReminder.classList.remove('fa-toggle-on');
            reminderField.classList.add('hidden');
            btnToggleEditReminder.classList.add('fa-toggle-off');
            btnToggleEditReminder.classList.remove('fa-toggle-on');
            editReminderField.classList.add('hidden');
            App.reminderToggle = false;
        }
    }
    /* End functions to submit new task */

    /* Functions to update tasks and lists */
    function updateListedTask(list, task) {
        function updateTask() {
            var selectedListId = formEditTask.listSelect.options[formEditTask.listSelect.selectedIndex].dataset.id;
            clone.name = nameEditTask.value;
            if (clone.name === '')
            {clone.name = 'Naamloze Taak';}
            clone.priority = 'priority-' + App.currentPriority;
            clone.note = noteEditTask.value;
            clone.place = locationEditTask.value;
            if (App.deadlineToggle === true) {
                var hour = Utils.getHourFromTime(editTimePicker.value);
                var minutes = Utils.getMinutesFromTime(editTimePicker.value);
                clone.endDate = new Date(editDatePicker.value + 'T' + hour + ':' + minutes+'Z');
                if (Object.prototype.toString.call(clone.endDate) !== '[object Date]' || isNaN(clone.endDate)) {
                    clone.endDate = "none";
                }
                toggleDeadline();
            } else {
                clone.endDate = 'none';
            }
            if (clone.endDate !== "none" && App.reminderToggle === true) {
                var value = editReminderValue.value;
                var valueType = formEditTask.remindervaluetype.options[formEditTask.remindervaluetype.selectedIndex].value;
                clone.remindDate = clone.endDate.getTime() - Utils.getTimeInMs(value, valueType);
                clone.remindValue = value;
                clone.remindType = valueType;
            } else {
                clone.remindDate = "none";
            }
            clone.updated = new Date().getTime();
            App.todoDBContext.deleteListedElement(list, task);
            if (selectedListId === 'no-list') {
                clone.cat = catEditTask.value;
                App.todoDBContext.addElement(clone);
            } else {
                var selectedList = App.todoDBContext.getElement(selectedListId);
                selectedList.open = true;
                App.todoDBContext.addListedElement(selectedList, clone);
            }
            window.location.href = '#/';
            btnSubmitEditTask.removeEventListener('click', updateTask);
            btnCloseEditTask.removeEventListener('click', closeTaskEdit);
            showFeedback('Taak: "' + clone.name + '" is aangepast');
        }
        function closeTaskEdit() {
            window.location.href = '#/';
            btnSubmitEditTask.removeEventListener('click', updateTask);
            btnCloseEditTask.removeEventListener('click', closeTaskEdit);
        }

        window.location.href = '#/edit';
        var clone = task;
        taskEditor.classList.add('hidden');
        listEditor.classList.add('hidden');
        taskEditor.classList.remove('hidden');
        catEditTask.value = clone.cat;
        if (clone.endDate !== 'none') {
            App.deadlineToggle = true;
            btnToggleEditDeadline.classList.remove('fa-toggle-off');
            btnToggleEditDeadline.classList.add('fa-toggle-on');
            editTimeField.classList.remove('hidden');
            var endDate = new Date(clone.endDate);
            editTimePicker.value = Utils.createTimeString(endDate);
            editDatePicker.value = Utils.createDateString(endDate);
        } else {
            App.deadlineToggle = false;
            btnToggleEditDeadline.classList.add('fa-toggle-off');
            btnToggleEditDeadline.classList.remove('fa-toggle-on');
            editTimeField.classList.add('hidden');
        }
        if (clone.remindDate !== 'none') {
            App.reminderToggle = true;
            btnToggleEditReminder.classList.remove('fa-toggle-off');
            btnToggleEditReminder.classList.add('fa-toggle-on');
            editReminderField.classList.remove('hidden');
            editReminderValue.value = clone.remindValue;
            setTimeout(function() {Utils.selectItemByValue(editReminderValueType, clone.remindType);}, 10);
        } else {
            App.reminderToggle = false;
            btnToggleEditReminder.classList.add('fa-toggle-off');
            btnToggleEditReminder.classList.remove('fa-toggle-on');
            editReminderField.classList.add('hidden');
            editReminderValue.value = 0;
        }

        nameEditTask.value = clone.name;
        setTimeout(function() {Utils.selectItemByValue(editListSelection, list.id);}, 10);
        setPriority(Utils.getPriorityFromString(clone.priority));
        noteEditTask.value = clone.note;
        locationEditTask.value = clone.place;

        btnSubmitEditTask.addEventListener('click', updateTask);
        btnCloseEditTask.addEventListener('click', closeTaskEdit);

    }
    function updateElement(element) {

        function updateTask() {
            var selectedListId = formEditTask.listSelect.options[formEditTask.listSelect.selectedIndex].dataset.id;
            clone.name = nameEditTask.value;
            if (clone.name === '')
            {clone.name = 'Naamloze Taak';}
            clone.priority = 'priority-' + App.currentPriority;
            clone.note = noteEditTask.value;
            clone.place = locationEditTask.value;
            if (App.deadlineToggle === true) {
                var hour = Utils.getHourFromTime(editTimePicker.value);
                var minutes = Utils.getMinutesFromTime(editTimePicker.value);
                clone.endDate = new Date(editDatePicker.value + 'T' + hour + ':' + minutes+'Z');
                if (Object.prototype.toString.call(clone.endDate) !== '[object Date]' || isNaN(clone.endDate)) {
                    clone.endDate = "none";
                }
                toggleDeadline();
            } else {
                clone.endDate = 'none';
            }
            if (clone.endDate !== "none" && App.reminderToggle === true) {
                var value = editReminderValue.value;
                var valueType = formEditTask.remindervaluetype.options[formEditTask.remindervaluetype.selectedIndex].value;
                clone.remindDate = clone.endDate.getTime() - Utils.getTimeInMs(value, valueType);
                clone.remindValue = value;
                clone.remindType = valueType;
            } else {
                clone.remindDate = "none";
            }
            clone.updated = new Date().getTime();
            App.todoDBContext.deleteElementById(element.id);
            if (selectedListId === 'no-list') {
                clone.cat = catEditTask.value;
                App.todoDBContext.addElement(clone);
            } else {
                var selectedList = App.todoDBContext.getElement(selectedListId);
                selectedList.open = true;
                App.todoDBContext.addListedElement(selectedList, clone);
            }
            window.location.href = '#/';
            btnSubmitEditTask.removeEventListener('click', updateTask);
            btnCloseEditTask.removeEventListener('click', closeTaskEdit);
            showFeedback('Taak: "' + clone.name + '" is aangepast');
        }

        function closeTaskEdit() {
            window.location.href = '#/';
            btnSubmitEditTask.removeEventListener('click', updateTask);
            btnCloseEditTask.removeEventListener('click', closeTaskEdit);
        }

        function updateList() {
            clone.name = nameEditList.value;
            if (clone.name === '')
            {clone.name = 'Naamloze Lijst';}
            clone.cat = tagsEditList.value;
            clone.updated = new Date().getTime();
            App.todoDBContext.deleteElementById(element.id);
            App.todoDBContext.addElement(clone);
            window.location.href = '#/';
            btnSubmitEditList.removeEventListener('click', updateList);
            btnCloseEditList.removeEventListener('click', closeListEdit);
            showFeedback('Lijst: "' + clone.name + '" is aangepast');
        }

        function closeListEdit() {
            window.location.href = '#/';
            btnSubmitEditList.removeEventListener('click', updateList);
            btnCloseEditList.removeEventListener('click', closeListEdit);
        }

        window.location.href = '#/edit';
        var clone = element;
        taskEditor.classList.add('hidden');
        listEditor.classList.add('hidden');
        if (clone.type === "task") {
            taskEditor.classList.remove('hidden');
            if (clone.endDate !== 'none') {
                App.deadlineToggle = true;
                btnToggleEditDeadline.classList.remove('fa-toggle-off');
                btnToggleEditDeadline.classList.add('fa-toggle-on');
                editTimeField.classList.remove('hidden');
                var endDate = new Date(clone.endDate);
                editTimePicker.value = Utils.createTimeString(endDate);
                editDatePicker.value = Utils.createDateString(endDate);
            } else {
                App.deadlineToggle = false;
                btnToggleEditDeadline.classList.add('fa-toggle-off');
                btnToggleEditDeadline.classList.remove('fa-toggle-on');
                editTimeField.classList.add('hidden');
            }
            if (clone.remindDate !== 'none') {
                App.reminderToggle = true;
                btnToggleEditReminder.classList.remove('fa-toggle-off');
                btnToggleEditReminder.classList.add('fa-toggle-on');
                editReminderField.classList.remove('hidden');
                editReminderValue.value = clone.remindValue;
                setTimeout(function() {Utils.selectItemByValue(editReminderValueType, clone.remindType);}, 10);

            } else {
                App.reminderToggle = false;
                btnToggleEditReminder.classList.add('fa-toggle-off');
                btnToggleEditReminder.classList.remove('fa-toggle-on');
                editReminderField.classList.add('hidden');
                editReminderValue.value = 0;
            }

            nameEditTask.value = clone.name;
            setPriority(Utils.getPriorityFromString(clone.priority));
            noteEditTask.value = clone.note;
            locationEditTask.value = clone.place;
            catEditTask.value = clone.cat;


            btnSubmitEditTask.addEventListener('click', updateTask);
            btnCloseEditTask.addEventListener('click', closeTaskEdit);

        }
        if (clone.type === "list") {
            listEditor.classList.remove('hidden');

            nameEditList.value = clone.name;
            tagsEditList.value = clone.cat;

            btnSubmitEditList.addEventListener('click', updateList);
            btnCloseEditList.addEventListener('click', closeListEdit);
        }

    }
    /* End functions to update tasks and lists */

    /* Functions to complete tasks */
    function toggleCompleteTask(task) {
        var clone = task;
        clone.completed = !clone.completed;
        App.todoDBContext.deleteElementById(task.id);
        App.todoDBContext.addElement(clone);
        App.updateUI();
    }
    function toggleCompleteListedTask(task, list) {
        var clone = task;
        clone.completed = !clone.completed;
        App.todoDBContext.deleteListedElementById(list, task.id);
        App.todoDBContext.addListedElement(list, task);
        App.updateUI();
    }
    /* End functions to complete tasks */

    /* Functions to delete tasks and lists */
    function removeElementById(elementId) {
        App.todoDBContext.deleteElementById(elementId);
        App.updateUI();
    }
    function removeListedElementById(list, elementId) {
        App.todoDBContext.deleteListedElementById(list, elementId);
        App.updateUI();
    }
    /* End functions to delete tasks and lists */

    /* Form control functions */
    function toggleListOpen(list) {
        list.open = !list.open;
        App.updateUI();
    }
    function toggleAllOff() {
        if (App.deadlineToggle === true){
            toggleDeadline();}
        if (App.reminderToggle === true){
            toggleReminder();}
    }
    function closeNewTask() {
        App.clearForms();
        formNewTask.classList.add('hidden');
    }
    function closeNewList() {
        App.clearForms();
        formNewList.classList.add('hidden');
    }
    /* End form control functions */

    /* HTML generating functions */
    function getHTMLForTaskById(taskId) {
        var task = App.todoDBContext.getElement(taskId);
        var htmlContent = '';
        if (task !== null) {
            var htmlTags = getHTMLForTaskTags(task);
            var htmlNote = getHTMLForTaskNote(task);
            var htmlLocation = getHTMLForTaskLocation(task);
            var htmlDate = getHTMLForTaskDate(task);
            var htmlReminder = getHTMLForReminder(task);
            if (task.completed === false) {
                htmlContent += '<article class="element task ' + task.priority + '"" data-id="' + task.id + '">' + '<span class="task-icon"><i class="fa fa-square-o"></i></span>' + '<p class="task-content">' + task.name + '</p>' + htmlTags + htmlNote + htmlLocation + htmlDate + htmlReminder + '<span class="element-edit"><i class="fa fa-pencil"></i></span>' + '<span class="element-remove"><i class="fa fa-remove"></i></span>' + '</article>';
            } else {
                htmlContent += '<article class="element task completed" data-id="' + task.id + '">' + '<span class="task-icon"><i class="fa fa-check-square-o"></i></span>' + '<p class="task-content">' + task.name + '</p>' + htmlNote + htmlLocation + htmlDate + '<span class="element-edit"><i class="fa fa-pencil"></i></span>' + '<span class="element-remove"><i class="fa fa-remove"></i></span>' + '</article>';
            }
        }
        return htmlContent;
    }
    function getHTMLForListById(listId) {
        var list = App.todoDBContext.getElement(listId);
        var htmlContent = '';
        if (list !== null) {
            var htmlTags = getHTMLForListTags(list);
            htmlContent += '<article class="element list" data-id="' + list.id + '">' + '<span class="list-icon"><i class="fa fa-list"></i></span>' + '<p class="list-content">' + list.name + '</p>' + htmlTags + '<span class="element-edit"><i class="fa fa-pencil"></i></span>' + '<span class="element-remove"><i class="fa fa-remove"></i></span>';
            var listItems = App.todoDBContext.getListedElements(list);
            if (listItems !== null && listItems.length > 0 && list.open === true) {
                _.each(listItems, function(listItem) {
                    var htmlNote = getHTMLForTaskNote(listItem);
                    var htmlLocation = getHTMLForTaskLocation(listItem);
                    var htmlDate = getHTMLForTaskDate(listItem);
                    var htmlReminder = getHTMLForReminder(listItem);
                    if (listItem.completed === false) {
                        htmlContent += '<article class="listed-task ' + listItem.priority + '"" data-id="' + listItem.id + '"data-list="' + listId + '">' + '<span class="listed-task-icon"><i class="fa fa-square-o"></i></span>' + '<p class="listed-task-content">' + listItem.name + '</p>' + htmlNote + htmlLocation + htmlDate + htmlReminder + '<span class="listed-task-edit"><i class="fa fa-pencil"></i></span>' + '<span class="listed-task-remove"><i class="fa fa-remove"></i></span>' + '</article>';
                    } else {
                        htmlContent += '<article class="listed-task completed"" data-id="' + listItem.id + '"data-list="' + listId + '">' + '<span class="listed-task-icon"><i class="fa fa-check-square-o"></i></span>' + '<p class="listed-task-content">' + listItem.name + '</p>' + htmlNote + htmlLocation + htmlDate + '<span class="listed-task-edit"><i class="fa fa-pencil"></i></span>' + '<span class="listed-task-remove"><i class="fa fa-remove"></i></span>' + '</article>';
                    }
                });
            }
            htmlContent += '</article>';
        }

        return htmlContent;
    }
    function getHTMLForTaskNote(task) {
        var htmlContent = '';
        if (task.note !== '') {

            htmlContent = '<div class="taskinfo note"><span><i class="fa fa-exclamation"></i>' + task.note + '</span></div>';

        }

        return htmlContent;
    }
    function getHTMLForTaskLocation(task) {
        var htmlContent = '';
        if (task.place !== '') {

            htmlContent = '<div class="taskinfo location"><span><i class="fa fa-map-marker"></i>' + task.place + '</span></div>';

        }

        return htmlContent;
    }
    function getHTMLForReminder(task) {
        var htmlContent = '';
        if (task.remindDate !== 'none' && Utils.enableReminder(task.remindDate)) {

            htmlContent = '<div class="taskinfo reminder"><span><i class="fa fa-bell"></i>Herinnering</span></div>';

        }

        return htmlContent;
    }
    function getHTMLForTaskDate(task) {
        var htmlContent = '';
        if (task.endDate !== 'none') {
            var date = new Date(task.endDate);

            htmlContent = '<div class="taskinfo date"><span><i class="fa fa-calendar"></i>' + Utils.dateToString(date) + '</span></div>';
        }
        return htmlContent;
    }
    function getHTMLForTaskTags(task){
        var tag = task.cat;
        var htmlContent = '<div class="tags">';
        if (tag !== ''){
            htmlContent += '<span class="tag"><i class="fa fa-tag"></i>' + tag + '</span>';}
        htmlContent += '</div>';
        if (htmlContent === '<div class="tags"></div>'){
            htmlContent = '';}
        return htmlContent;
    }
    function getHTMLForListTags(list) {
        var tag = list.cat;
        var htmlContent = '<div class="tags">';
            if (tag !== ''){
                htmlContent += '<span class="tag"><i class="fa fa-tag"></i>' + tag + '</span>';}
        htmlContent += '</div>';
        if (htmlContent === '<div class="tags"></div>'){
            htmlContent = '';}
        return htmlContent;
    }
    /* End HTML generating functions */

    /* Rendering functions */
    function createElementsInContainer(container) {
        if (container !== null) {

            var elements = App.todoDBContext.getElements();
            container.innerHTML = '';
            var htmlContent = '';
            _.each(elements, function(element) {
                if (element.type === 'task') {
                    htmlContent += getHTMLForTaskById(element.id);
                } else if (element.type === 'list') {
                    htmlContent += getHTMLForListById(element.id);
                }
            });
            container.innerHTML = htmlContent;
        }
    }
    function createRemindersInContainer(container) {
        if (container !== null) {

            var elements = App.todoDBContext.getElements();
            container.innerHTML = '';
            var htmlContent = '';
            _.each(elements, function(element) {
                if (element.type === 'task') {
                    if (element.remindDate !== 'none' && Utils.enableReminder(element.remindDate))
                    {htmlContent += getHTMLForTaskById(element.id);}
                } else if (element.type === 'list') {
                    var listItems = App.todoDBContext.getListedElements(element);
                    var listId = element.id;
                    _.each(listItems, function (listItem){
                        if (listItem.remindDate !== 'none' && Utils.enableReminder(listItem.remindDate))
                        {
                            var htmlNote = getHTMLForTaskNote(listItem);
                            var htmlLocation = getHTMLForTaskLocation(listItem);
                            var htmlDate = getHTMLForTaskDate(listItem);
                            var htmlReminder = getHTMLForReminder(listItem);
                            if (listItem.completed === false) {
                                htmlContent += '<article class="listed-task ' + listItem.priority + '"" data-id="' + listItem.id + '"data-list="' + listId + '">' + '<span class="listed-task-icon"><i class="fa fa-square-o"></i></span>' + '<p class="listed-task-content">' + listItem.name + '</p>' + htmlNote + htmlLocation + htmlDate + htmlReminder + '<span class="listed-task-edit"><i class="fa fa-pencil"></i></span>' + '<span class="listed-task-remove"><i class="fa fa-remove"></i></span>' + '</article>';
                            } else {
                                htmlContent += '<article class="listed-task completed"" data-id="' + listItem.id + '"data-list="' + listId + '">' + '<span class="listed-task-icon"><i class="fa fa-check-square-o"></i></span>' + '<p class="listed-task-content">' + listItem.name + '</p>' + htmlNote + htmlLocation + htmlDate + '<span class="listed-task-edit"><i class="fa fa-pencil"></i></span>' + '<span class="listed-task-remove"><i class="fa fa-remove"></i></span>' + '</article>';
                            }
                        }
                    });
                }
            });
            container.innerHTML = htmlContent;
        }
    }
    /* End rendering functions */

    /* Function to clear all stored tasks and lists */
    function clear() {
        if (App.todoDBContext.getLocalStorageAlert())
        {
            showPopup('Wil je alle gegevens verwijderen?',
                function(confirmation){
                    if(confirmation){
                        var elements = App.todoDBContext.getElements();
                        if (elements !== null && elements.length > 0) {
                            _.each(elements, function (element) {
                                App.todoDBContext.deleteElement(element);
                            });
                        }
                    }
                });
        }else{
            var elements = App.todoDBContext.getElements();
            if (elements !== null && elements.length > 0) {
                _.each(elements, function (element) {
                    App.todoDBContext.deleteElement(element);
                });
            }
        }
    }
    /* End function to clear all stored tasks and lists */

    /* Initialise the Application */
    App.init();
    /* End initialise the Application */
};