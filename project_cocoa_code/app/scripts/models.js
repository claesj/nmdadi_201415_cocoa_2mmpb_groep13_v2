/*
 * Created by: Jens Claes & Stijn De Rammelaere
 * Last Update: 14-01-2015
 * Name: services.js
 * Project: DoYouRemember?!
 * Description: Webapp - To Do List
 * Site: www.doyouremember.eu
 */

/* exported List, Task */
function List(){
    'use strict';
    this.id = -1;
    this.type = "list";
    this.name = "listname";
    this.open = true;
    this.cat = null;
    this.data = [];
    this.created = null;
    this.updated = null;
    this.toString = function(){
        return this.name;
    };
}

function Task(){
    'use strict';
    this.id = -1;
    this.type = "task";
    this.name = "taskname";
    this.priority = "priority-zero";
    this.cat = null;
    this.completed = false;
    this.created = null;
    this.updated = null;
    this.note = "";
    this.place = "";
    this.endDate = null;
    this.remindDate = null;
    this.remindValue = null;
    this.remindType = null;
    this.toString = function(){
        return this.name;
    };
}