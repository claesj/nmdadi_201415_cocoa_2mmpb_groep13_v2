/*
 * Created by: Jens Claes & Stijn De Rammelaere
 * Last Update: 26-12-2014
 * Name: services.js
 * Project: DoYouRemember?!
 * Description: Webapp - To Do List
 * Site: www.doyouremember.eu
 */

/* exported TodoDBContext */
var TodoDBContext = {
    init:function(connString){
        'use strict';
        this.connString = connString;
        this.AppData = {
            "information":{
                "title":"Do You Remember",
                "version":"1.1",
                "modified":"01-12-2014",
                "author":"Jens Claes & Stijn Derammelaere"
            },
            "elements":[],
            /*
             * 0 = feedback
             * 1 = delete
             * 2 = reminder
             * 3 = localstorage
             * */
            "settings":[true,true,true,true]
        };
        if(Utils.store(this.connString) !== null){
            this.AppData = Utils.store(this.connString);
        }else{
            Utils.store(this.connString, this.AppData);
        }
    },
    getElements:function(){
        'use strict';
        var elements = this.AppData.elements;
        if(elements === null)
        {return null;}
        elements = _.sortBy(elements, 'created');
        return elements;
    },
    getListedElements:function(list){
        'use strict';
        var elements = list.data;
        if(elements === null)
        {return null;}
        elements = _.sortBy(elements, 'created');
        return elements;
    },
    getElement:function(id){
        'use strict';
        var element = _.find(this.AppData.elements, function(obj){
            return obj.id === id;
        });

        return element;
    },
    getListedElement:function(list,elementId){
        'use strict';
        var element = _.find(list.data, function(obj){
            return obj.id === elementId;
        });

        return element;
    },
    addElement:function(element){
        'use strict';
        this.AppData.elements.push(element);
        this.save();

        return 1;
    },
    addListedElement:function(list,element){
        'use strict';
        list.data.push(element);
        this.save();

        return 1;
    },
    deleteElementById:function(id){
        'use strict';
        var index = _.findIndex(this.AppData.elements, function(obj){
            return obj.id === id;
        });

        if(index === -1)
        {return 0;}

        this.AppData.elements.splice(index, 1);
        this.save();

        return 1;
    },
    deleteListedElementById:function(list,id){
        'use strict';
        var index = _.findIndex(list.data, function(obj){
            return obj.id === id;
        });

        if(index === -1)
        {return 0;}

        list.data.splice(index, 1);
        this.save();

        return 1;
    },
    deleteElement:function(element){
        'use strict';
        return this.deleteElementById(element.id);
    },
    deleteListedElement:function(list,element){
        'use strict';
        return this.deleteListedElementById(list,element.id);
    },
    setFeedbackAlert: function (bool){
        'use strict';
        this.AppData.settings[0] = bool;
        this.save();

        return 1;
    },
    setDeleteAlert: function (bool){
        'use strict';
        this.AppData.settings[1] = bool;
        this.save();

        return 1;
    },
    setLocalStorageAlert: function (bool){
        'use strict';
        this.AppData.settings[3] = bool;
        this.save();

        return 1;
    },
    getFeedbackAlert: function (){
        'use strict';
       return this.AppData.settings[0];
    },
    getDeleteAlert: function (){
        'use strict';
        return this.AppData.settings[1];
    },
    getLocalStorageAlert: function (){
        'use strict';
        return this.AppData.settings[3];
    },
    save:function(){
        'use strict';
        Utils.store(this.connString, this.AppData);
    }
};