/*
 * Created by: Jens Claes & Stijn De Rammelaere
 * Last Update: 14-01-2015
 * Name: utilities.js
 * Project: DoYouRemember?!
 * Description: Webapp - To Do List
 * Site: www.doyouremember.eu
 */

/* exported Utils */
var Utils = {
    guid: function(){
        'use strict';
        var i, random;
        var uuid = '';

        for (i = 0; i < 32; i++){
            random = Math.random() * 16 | 0;
            if (i === 8 || i === 12 || i === 16 || i === 20) {
                uuid += '-';
            }
            uuid += (i === 12 ? 4 : (i === 16 ? (random & 3 | 8) : random)).toString(16);
        }
        return uuid;
    },
    store: function (namespace, data) {
        'use strict';
        if (arguments.length > 1) {
            return localStorage.setItem(namespace, JSON.stringify(data));
        } else {
            var store = localStorage.getItem(namespace);
            return (store && JSON.parse(store)) || null;
        }
    },
    getPriorityFromString: function(string){
        'use strict';
        var split = string.split('-');
        return split[1];
    },
    enableReminder: function (reminder){
        'use strict';
        var d = new Date().getTime();
        return d >= (reminder - 3600000);
    },
    getTimeInMs: function(value, type){
        'use strict';
        var multi;
        var val = value;

        if (isNaN(val))
        {val = 0;}

        switch (type){
            case 'minuten':
                multi = 60000;
                break;
            case 'uren':
                multi = 3600000;
                break;
            case 'dagen':
                multi = 86400000;
                break;
            case 'weken':
                multi = 604800000;
                break;
            default:
                multi = 0;
                break;
        }
        return val * multi;
    },
    createTimeString: function (date) {
        'use strict';
        var hour = date.getHours();
        var minutes = date.getMinutes();
        hour = hour - 1;
        if (hour === -1)
        {hour = 23;}
        if (hour <10)
        {hour = '0' + hour;}
        if (minutes<10)
        {minutes = '0' + minutes;}

        return hour + ':' + minutes;
    },
    createDateString: function (date) {
        'use strict';
        var year = date.getFullYear();
        var month = date.getMonth() + 1;
        var day = date.getDate();

        if (month <10)
        {month = '0' + month;}
        if (day<10)
        {day = '0' + day;}

        return year + '-' + month + '-' + day;
    },
    dateToString: function (date){
        'use strict';
        var day = date.getDate();
        var month = date.getMonth() + 1;
        var year = date.getFullYear();
        var hour = date.getHours() -1;
        if(hour === -1)
        {
            hour = 23;
        }
        var minutes = date.getMinutes();
        if (minutes < 10)
        {minutes = '0' + minutes;}

        return hour + ':' + minutes + '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;' + day + '/' + month + '/' + year;

    },
    getHourFromTime: function (time) {
        'use strict';
        if(time !== '') {
            var timeSplit = time.split(':');
            var hour = parseInt(timeSplit[0]);
            if(hour < 10){
                hour = '0' + hour;
            }
            return hour;
        }else {
            return '00';
        }
    },
    getMinutesFromTime: function (time) {
        'use strict';
        if(time !== '') {
            var timeSplit = time.split(':');
            return timeSplit[1];
        }else {
            return '00';
        }
    },
    selectItemByValue: function (elmnt, value){
        'use strict';
        for(var i=0; i < elmnt.options.length; i++)
        {
            if(elmnt.options[i].value === value) {
                elmnt.selectedIndex = i;
                break;
            }
        }
    }
};