var gulp = require('gulp'),
    plugins = require('gulp-load-plugins')(),
    del = require('del'),
    runSequence = require('run-sequence'),
    browserSync = require('browser-sync'),
    reload = browserSync.reload;

//Clean the .tmp folder and dist folder (keep the .gitkeep)
gulp.task('clean', del.bind(null, ['.tmp', 'dist/*', '!dist/.gitkeep']));

// Copy all files from app to dist, except all html documents
gulp.task('copy', function(){
    return gulp.src([
        'app/*',
        '!app/**/*.html'
    ],{dot:true})
        .pipe(gulp.dest('dist'))
        .pipe(plugins.size({title:'copy'}));
});

// Copy all css images from app to dist
gulp.task('copycssimages', function(){
    return gulp.src([
        'app/styles/images/**/*',
        '!app/styles/images/**/.gitkeep'
    ],{dot:true})
        .pipe(gulp.dest('dist/styles/images'))
        .pipe(plugins.size({title:'copy'}));
});

// Copy all css fonts from app to dist
gulp.task('copycssfonts', function(){
    return gulp.src([
        'app/styles/fonts/**/*',
        '!app/styles/fonts/**/.gitkeep'
    ],{dot:true})
        .pipe(gulp.dest('dist/styles/fonts'))
        .pipe(plugins.size({title:'copy'}));
});

// Copy all images (content) from app to dist
gulp.task('copyimages', function(){
    return gulp.src([
        'app/images/**/*',
        '!app/images/**/.gitkeep'
    ],{dot:true})
        .pipe(gulp.dest('dist/images'))
        .pipe(plugins.size({title:'copy'}));
});

// Copy all docs (content) from app to dist
gulp.task('copydocs', function(){
    return gulp.src([
        'app/docs/**/*',
        '!app/docs/**/.gitkeep'
    ],{dot:true})
        .pipe(gulp.dest('dist/images'))
        .pipe(plugins.size({title:'copy'}));
});

//Check my Custom JavaScript files --> Syntax check
gulp.task('jshint', function(){
    return gulp.src('app/scripts/**/*.js')
        .pipe(plugins.jshint())
        .pipe(plugins.jshint.reporter('jshint-stylish'));
});

//Get the assets from all the html documents and optimize them
gulp.task('html', function(){
    var assets = plugins.useref.assets({searchFor:'{.tmp, app}'});

    return gulp.src('app/**/*.html')
        .pipe(assets)
        .pipe(plugins.if('*.js', plugins.uglify()))
        .pipe(plugins.if('*.css', plugins.csso()))
        .pipe(assets.restore())
        .pipe(plugins.useref())
        .pipe(gulp.dest('dist'))
        .pipe(plugins.size({title:'html'}));
});

// Build and serve the output from the dist build
// 1. The task default will be executed first
// 2. After successfully executed the previous task
// A Node server will be launched on port
// EXEC: gulp serve:dist
gulp.task('serve:dist', ['default'], function () {
    browserSync({
        browser:'Chrome',
        notify: false,
        logPrefix: 'WSK',
        https: false,
        server: 'dist',
        port:8082
    });
});

// Build Production Files, the Default Task
gulp.task('default', ['clean'], function (cb) {
    runSequence('copy', ['html', 'copycssimages', 'copycssfonts', 'copyimages', 'copydocs'], cb);
});